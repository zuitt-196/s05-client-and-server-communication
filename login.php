<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
</head>

<body>
    <?php session_start(); ?>

    <?php if (isset($_SESSION['info'])) : ?>
        <?php foreach ($_SESSION['info'] as $email => $data) : ?>
            <div>
                <form action="./logout.php" method="GET">
                    <input type="hidden" name="action" value="clear">
                    <p>Hello <?php echo $data->email; ?></p>
                    <button type="submit">Logout</button>
                </form>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>


</body>

</html>