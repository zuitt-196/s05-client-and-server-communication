<?php session_start();
/*session_start() is also added in the server.php so it is also included in the current session*/

//session variable
var_dump($_SESSION['info']);


//Create a TaskList class to hold the methods in adding,viewing, editing and Deleting task

class Login
{
	//add task
	public function loginPerson($email)
	{
		$newTask = (object)[
			'email' => $email,

		];

		/*Session variables are set with PHP global variables : $_SESSION*/

		if ($_SESSION['info'] === null) {
			$_SESSION['info'] = array();
		}

		//The $newTask will be added in the $_SESSION['info']variable
		array_push($_SESSION['info'], $newTask);
	}

	//DELETE ALL DATA
	public function clear()
	{
		//removes all the data associated with the current session
		session_destroy();
	}
}

//Instatiation of TaskList
// taskList is instantiated from the the TaskList() class to have access with its method
$login = new Login();

//Create an if statement to handle the client request and identify the action needed to execute.
//This if statement will handle the action by the user
//taskLists methodd will invoke on the action

//Adding task
if ($_POST['action'] === 'add') {
	$login->loginPerson($_POST['email']);
}



// //header()
// //This is used to send raw HTTP header, but this can also be used to redirect us on specific location
// //it will redirect us to the index file upon sending the request.
header("Location: ./login.php");
