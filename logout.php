<?php
session_start();
function clear()
{
    //removes all the data associated with the current session
    session_destroy();
}


if ($_GET['action'] === 'clear') {
    # code...
    clear();
}


header("Location: ./index.php");
